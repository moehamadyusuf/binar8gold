const express = require('express');
const app = express();
const { User } = require('./models')

app.use(express.urlencoded({ extended: false }));

/* INPUT DATA EMAIL & PASSWORD */
app.get('/register', (req, res) => {
    const in_role = req.body.role
    const in_username = req.body.username
    const in_address = req.body.address
    const in_email = req.body.email
    const in_password = req.body.password

    User.create({
        role: in_role,
        username: in_username,
        address: in_address,
        email: in_email,
        password: in_password
    })
        .then(User => {
            console.log(User)
            res.json(User);
        })
        .catch(() => {
            res.send("gagal menambahkan data baru")
        })
});

app.get('/allUser', (req, res) => {
    User.findAll()
        .then(User => {
            console.log(User)
            res.json(User);
    })
    .catch(() => {
        res.send("gagal menampilkan data")
    })
})

app.listen(4444, () => {
    console.log("server running : localhost: 4444")
})

